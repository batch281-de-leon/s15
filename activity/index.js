	
	console.log("Hello, World!");

	let firstName = 'First Name: ' + 'Mikaella';
	console.log(firstName);

	let lastName = 'Last Name: ' + 'De Leon';
	console.log(lastName);

	let age = 'Age: ' + 24;
	console.log(age);

	let category = "Hobbies:"
	console.log(category);

	let hobbies = ['Surfing web','Practice singing','Watching movies']
	console.log(hobbies);

	let address = "Home Address:"
	console.log(address);

	let homeAddress = {
		houseNumber: 'Blk 16A Lot 28',
		street: 'Mt. Apo',
		city: 'San Pedro',
		state: 'Laguna'
	}
	console.log(homeAddress);

/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

	let fullName = "Steve Rogers";
	console.log("My full name is " + fullName);

	let currentAge = 40;
	console.log("My current age is: " + currentAge);
	
	let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
	console.log("My Friends are: " + friends);
	console.log(friends);

	let profile = {

		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false

	}
	console.log("My Full Profile: ")
	console.log(profile);

	let name = "Bucky Barnes";
	console.log("My bestfriend is: " + name);

	const lastLocation = "Arctic Ocean";
	console.log("I was found frozen in: " + lastLocation);

